/**
 * @copyright 2019 - Max Bebök
 * @author Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
 */

export function radToDeg(rad) {
    return rad * 180.0 / Math.PI;
}

export function degToRad(deg) {
    return deg * Math.PI / 180.0;
}