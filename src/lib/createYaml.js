/**
 * @copyright 2019 - Max Bebök
 * @author Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
 */

import yaml from 'js-yaml';

const FILE_VERSION = 1;

function vectorEquals(arr, val) {
    return arr[0] == val && arr[1] == val && arr[2] == val;
}

function arrayToFloat(arr) 
{
    if(arr)
    {
        for(let i=0; i<arr.length; ++i) {
            arr[i] = parseFloat(arr[i]);
        }
    }
}

function normalizeBox(entry) 
{
    delete entry.radius;
    if(entry.rotation) 
    {
        if(vectorEquals(entry.rotation, 0.0)) 
        {
            delete entry.rotation;
            delete entry.scale;
        }else{
            entry.scale = [1.0, 1.0, 1.0, 1.0];
        }
    } else {
        delete entry.scale;
    }
}

function normalizeMesh(entry) 
{
    normalizeSphere(entry);
    delete entry.radius;
}

function normalizeSphere(entry) 
{
    entry.scale = [...entry.sizeHalf];
    delete entry.sizeHalf;

    if(vectorEquals(entry.scale,1.0) && vectorEquals(entry.rotation, 0.0))
    {
        delete entry.scale;
        delete entry.rotation;
    }else{
        entry.scale[3] = 1.0;
    }
}

export default function createYaml(file, asText = true) 
{
    const fileData = JSON.parse(JSON.stringify(file)); // "copy"

    fileData.version = FILE_VERSION;

    if(!fileData.entries)
        fileData.entries = [];

    for(let chunk of fileData.entries) 
    {
        if(!chunk.entries)
            chunk.entries = [];

        for(let entry of chunk.entries) 
        {
            switch(entry.type)
            {
                case "box": normalizeBox(entry); break;
                case "sphere": normalizeSphere(entry); break;
                case "mesh": normalizeMesh(entry); break;
            }

            if(entry.rotation)
            {
                entry.rotation[0] *= -1;
                entry.rotation[1] *= -1;
            }

            if(Object.keys(entry.flags).length == 0) {
                delete entry.flags;
            }

            arrayToFloat(entry.position);            
            arrayToFloat(entry.sizeHalf);
            arrayToFloat(entry.rotation);
            arrayToFloat(entry.scale);
        }
    }

    return asText ? yaml.safeDump(fileData, {flowLevel: 6}) : fileData;
}