/**
 * @copyright 2019 - Max Bebök
 * @author Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
 */

export class ObjLoader
{
    constructor()
    {
        this.vertexArray = [];
        this.indexArray = [];
    }

    load(data)
    {
        const lines = data.split("\n");

        for (let i=0; i<lines.length; ++i) 
        {
            const line = lines[i].replace(/\r/g, '').trim();
            if(line.startsWith("#")) {
                continue;
            }
            
            const parts = line.split(" ");
            const data = parts.slice(1);

            switch(parts[0])
            {
                case "v":
                    this.vertexArray.push(
                        data.map(v => parseFloat(v))
                    );
                break;

                case "f":
                    this.indexArray.push(
                        data.map(v => parseInt(v.split("/")[0]) - 1)
                    );
                break;
            }
        }
    }

    createModel()
    {
        const mat = new THREE.MeshLambertMaterial({transparent: true});
        const geometry = new THREE.ConvexGeometry( this.vertexArray.map(v => new THREE.Vector3(v[0], v[1], v[2])) );
        return new THREE.Mesh( geometry, mat );
    }
}

export default function loadCSV(data)
{
    const loader = new CsvLoader();
    loader.load(data);
    return loader.createModel();
}