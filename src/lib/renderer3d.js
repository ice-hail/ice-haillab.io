/**
 * @copyright 2019 - Max Bebök
 * @author Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
 */

import loadCsv from './modelLoader/csv';

export default class Renderer3D 
{
    init(canvas) 
    {
        this.canvas = canvas;

        this.renderer = new THREE.WebGLRenderer({canvas});        
        this.scene = new THREE.Scene();

        this.camera =new THREE.PerspectiveCamera(45.0, this.aspectRatio, 0.1, 10000);
        this.camera.position.set(4.0, 4.0, 4.0);
        this.camera.lookAt(new THREE.Vector3(0.0, 0.0, 0.0));
        this.scene.add(this.camera);
        
        this.updateCanvasSize();

        const gridColor = 0x595959;
        const gridColorLight = 0x303030;
        const gridSize = 20;

        const gridBig = new THREE.GridHelper(gridSize, gridSize/2, gridColor, gridColor);
        gridBig.position.y = -0.0001;
        this.scene.add(gridBig);

        const gridMini = new THREE.GridHelper(gridSize, gridSize*2, gridColorLight, gridColorLight);
        gridMini.position.y = -0.0002;
        this.scene.add(gridMini);

        this.axes = new THREE.AxesHelper(gridSize);
        this.scene.add(this.axes);

        this.setupLighting();
        this.setupMaterials();

        this.controls = new THREE.OrbitControls(this.camera, canvas);

        this.update();
    }

    updateCanvasSize()
    {
        this.width = this.canvas.clientWidth;
        this.height = this.canvas.clientHeight;

        this.aspectRatio = this.width / this.height;

        this.renderer.setSize(this.width, this.height);
        this.renderer.setPixelRatio(window.devicePixelRatio || 1);

        this.camera.aspect = this.aspectRatio;
        this.camera.updateProjectionMatrix();
    }

    setupLighting()
    {
        const pointLight =new THREE.PointLight(0xFFFFFF);
        pointLight.position.x = -20;
        pointLight.position.y = 20;
        pointLight.position.z = -20;
        this.scene.add(pointLight);

        const pointLight2 = new THREE.PointLight(0xFFFFFF);
        pointLight2.position.x = 20;
        pointLight2.position.y = -10;
        pointLight2.position.z = 20;
        this.scene.add(pointLight2);

        const ambientLight = new THREE.AmbientLight(0xFFFFFF);
        ambientLight.intensity = 0.2;
        this.scene.add(ambientLight);
    }

    setupMaterials()
    {
        this.bumpMap = (new THREE.TextureLoader()).load("img/bump.png");

        this.materialDefault = new THREE.MeshNormalMaterial();
        this.materialClimb = new THREE.MeshNormalMaterial({
            bumpMap: this.bumpMap,
            bumpScale: 0.5
        });
    }

    addBox()
    {
        const box = new THREE.Mesh(new THREE.BoxGeometry(2.0, 2.0, 2.0), this.materialDefault);
        box.rotation.order = "ZYX";
        this.scene.add(box);
        return box;
    }

    addSphere()
    {
        const sphere = new THREE.Mesh(new THREE.SphereGeometry(1.0, 14.0, 14.0), this.materialDefault);
        sphere.rotation.order = "ZYX";
        this.scene.add(sphere);
        return sphere;
    }

    addMesh(mesh) 
    {
        mesh.material = this.materialDefault;
        mesh.rotation.order = "ZYX";
        this.scene.add(mesh);
        return mesh;
    }

    update()
    {
        this.renderer.render(this.scene, this.camera);
        requestAnimationFrame(() => this.update());
    }
};